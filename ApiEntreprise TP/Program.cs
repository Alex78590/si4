﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiEntreprise;

namespace ApiEntreprise_TP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employe> collectionEmploye = Persistance.ChargeEmploye();
            if (collectionEmploye == null)
            {
                collectionEmploye = new List<Employe>();
            }
            List<Service> collectionService = Persistance.ChargeService();
            if (collectionService == null)
            {
                collectionService = new List<Service>();
            }

            bool continuer = true;
            do
            {
                Console.WriteLine("Gestion des employés");
                Console.WriteLine();
                Console.WriteLine("1-Ajouter employe");
                Console.WriteLine("2-Afficher la liste des employes");
                Console.WriteLine("3-modifier employe");
                Console.WriteLine("4-Ajouter un service");
                Console.WriteLine("5-Afficher un service");
                Console.WriteLine("6-Supprimer un employe");
                Console.WriteLine("7-Afficher la somme des salaires");
                Console.WriteLine("8-Afficher le plus haut salaire");
                Console.WriteLine("0-Fermer le programme");
                
                string choix = Console.ReadLine();

                
                switch (choix)

                {
                    case "1":
                        Employe unEmploye = new Employe();
                        Console.WriteLine("Numero");
                        string chaineSaisie = Console.ReadLine();
                        unEmploye.NumEmploye = Convert.ToInt64(chaineSaisie);
                        Console.WriteLine("Nom");
                        unEmploye.NomEmploye = Console.ReadLine();
                        Console.WriteLine("Prenom");
                        unEmploye.PrenomEmploye = Console.ReadLine();
                        Console.WriteLine("Salaire");
                        chaineSaisie = Console.ReadLine();
                        unEmploye.SalaireEmploye = Convert.ToDecimal(chaineSaisie);
                        Console.WriteLine("Ajoutez un service existant à un employe");
                        chaineSaisie = Console.ReadLine();
                        foreach (Service serviceCourant in collectionService)
                        {
                            if (chaineSaisie==serviceCourant.NomService)
                            {
                                unEmploye.ServiceEmploye = serviceCourant;
                            }
                            else
                            {
                                Console.WriteLine("Veuillez saisir un service existant");
                            }
                        }
                        collectionEmploye.Add(unEmploye);



                        break;

                    case "2":

                        foreach (Employe employeCourant in collectionEmploye)
                        {
                            Console.WriteLine("Num de l'employe:{0}",employeCourant.NumEmploye);
                            Console.WriteLine("Nom de l'employe:{0}",employeCourant.NomEmploye);
                            Console.WriteLine("Prenom de l'employe:{0}",employeCourant.PrenomEmploye);
                            Console.WriteLine("Salaire de l'employe:{0}",employeCourant.SalaireEmploye);
                        }
                        foreach(Service serviceCourant in collectionService)
                        {
                            Console.WriteLine("Service de l'employe:{0}",serviceCourant.NomService);
                        }
                        Console.WriteLine("");
                        break;

                    case "3":
                        Console.WriteLine("Saisissez le nom de l'employe");
                        string valeurSaisie = Console.ReadLine();
                        Console.WriteLine("Saisissez le prenom de l'employe");
                        string valeurSaisie2 = Console.ReadLine();
                        foreach (Employe employeCourant in collectionEmploye)
                        {
                            if (employeCourant.NomEmploye == valeurSaisie && employeCourant.PrenomEmploye == valeurSaisie2)
                            {
                                collectionEmploye.Remove(employeCourant);
                                Employe unEmploye2 = new Employe();

                                Console.WriteLine("Numero");
                                string chaineSaisie2 = Console.ReadLine();
                                unEmploye2.NumEmploye = Convert.ToInt64(chaineSaisie2);
                                Console.WriteLine("Nom");
                                unEmploye2.NomEmploye = Console.ReadLine();
                                Console.WriteLine("Prenom");
                                unEmploye2.PrenomEmploye = Console.ReadLine();
                                Console.WriteLine("Salaire");
                                chaineSaisie = Console.ReadLine();
                                unEmploye2.SalaireEmploye = Convert.ToDecimal(chaineSaisie2);
                                Console.WriteLine("service:");
                                string chaineSaisie3 = Console.ReadLine();

                                foreach (Service serviceCourant in collectionService)
                                {
                                    if (chaineSaisie3==serviceCourant.NomService)
                                    {
                                        unEmploye2.ServiceEmploye = serviceCourant;
                                    }
                                }
                                if (employeCourant.NomEmploye != valeurSaisie || employeCourant.PrenomEmploye != valeurSaisie2)
                                {
                                    Console.WriteLine("Vous avez fait une erreur dans le nom ou le prénom");
                                }
                                collectionEmploye.Add(unEmploye2);
                                break;
                            }
                        }
                        break;

                    case "4":
                        Service newService = new Service();
                        Console.WriteLine("Nom du service");
                        newService.NomService = Console.ReadLine();
                        collectionService.Add(newService);
                        break;

                    case "5":
                        foreach(Service serviceCourant in collectionService)
                        {
                            Console.Write(serviceCourant.NomService);
                            Console.WriteLine();
                        }
                        break;

                    case "6":
                        foreach (Employe employeCourant in collectionEmploye) 
                        {
                            Console.Write(employeCourant.NumEmploye);
                            Console.Write(employeCourant.NomEmploye);
                            Console.Write(employeCourant.PrenomEmploye);
                            Console.Write(employeCourant.SalaireEmploye);
                        }
                        Console.WriteLine("Saisir le nom de l'employe a supprimer");
                        string nom = Console.ReadLine();
                        Console.WriteLine("Saisir le nom de l'employe a supprimer");
                        string prenom = Console.ReadLine();
                        foreach (Employe employeCourant in collectionEmploye)
                        {
                            if (employeCourant.NomEmploye == nom && employeCourant.PrenomEmploye==prenom)
                            {
                                collectionEmploye.Remove(employeCourant);
                                break;
                            }
                        }
                        break;


                    case "7":
                        decimal sommeSalaire = 0;
                        decimal hautSalaire = 0;
                        
                            foreach (Employe employeCourant in collectionEmploye)
                        {
                            sommeSalaire = employeCourant.SalaireEmploye + sommeSalaire;
                            if (employeCourant.SalaireEmploye>hautSalaire)
                            {
                                hautSalaire = employeCourant.SalaireEmploye;
                            }
                        }
                        Console.WriteLine("le plus haut saslaire est {0}", hautSalaire);
                        Console.WriteLine("la somme des salaires est de {0}", sommeSalaire);
                        break;

                    
                       

                    case "0":

                        continuer = false;
                        Persistance.Sauvegarde(collectionEmploye);
                        Persistance.Sauvegarde(collectionService);
                        break;
                    default:
                        break;
                         
                            
                            
                }

            }

            while (continuer == true);


       




        }
    }
}
