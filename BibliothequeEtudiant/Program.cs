﻿using System;
using System.Collections.Generic;
using ApiBibliotheque;



namespace Bibliotheque
{
    class Program
    {
        static void Main(string[] args)
        {
            #region CHARGER DONNEES

            List<Livre> collectionLivres = Persistance.ChargeLivre();
            List<Adherent> collectionAdherents = Persistance.ChargeAdherent();
            List<Emprunt> collectionEmprunts = Persistance.ChargeEmprunt();

            #endregion

            #region VARIABLES ==> Utilisées pour la structure WHILE (menu)
            int choix;
            bool sortir = false;
            #endregion

            #region VARIABLES ==> Utilisées pour stocker les données métiers
            string numAdherent, nomAdherent, prenomAdherent, emailAdherent, adresseAdherent, villeAdherent, codePostalAdherent;
            string titreLivre, auteur, isbn;
            int numLivre;
            string dateRetour;
            Livre unLivre = new Livre();
            Adherent unAdherent = new Adherent();
            #endregion

            #region VARIABLES (dénombrements et structures itératives)
            int nbElement, compteur;
            #endregion

            #region VARIABLES DIVERSES

            #endregion


            do
            {
                Console.WriteLine("");
                Console.WriteLine("------------------------------------------------------------------------");
                Console.WriteLine("0- Sortir du programme");
                Console.WriteLine("1- Ajouter un Livre");
                Console.WriteLine("2- Ajouter un adhérent");
                Console.WriteLine("3- Afficher la liste des adhérents");
                Console.WriteLine("4- Afficher la liste des livres");
                Console.WriteLine("5- Supprimer un adhérent");
                Console.WriteLine("6- Supprimer un livre");
                Console.WriteLine("7- Ajouter un emprunt");
                Console.WriteLine("8- Liste des emprunts");
                Console.WriteLine("9- Enregister le retour d'un livre");
                Console.WriteLine("------------------------------------------------------------------------");
                Console.WriteLine("");

                choix = Convert.ToInt32(Console.ReadLine());


                switch (choix)
                {
                    case 1:

                        /* A compléter
                         * 
                         * Ecrire les instructions qui :
                         * 
                         * permettent à l'utilisateur de saisir le numéro, le titre, le nom de l'auteur et l'ISBN d'un livre
                         * initialise une variable de type livre à partir des valeurs saisies
                         * ajoute le livre à la collection de livres
                         * */

                        Console.WriteLine("Saisir le numéro du livre");
                        unLivre.NumLivre = Console.ReadLine();                      
                        Console.WriteLine("Saisir le titre du livre");
                        unLivre.TitreLivre = Console.ReadLine();
                        Console.WriteLine("Saisir le nom de l'auteur");
                        unLivre.Auteur = Console.ReadLine();
                        Console.WriteLine("Saisir l'ISBN du livre");
                        unLivre.Isbn = Console.ReadLine();

                        collectionLivres.Add(unLivre);
                        Console.ReadKey();           
                        break;

                    case 2:

                        /* A compléter
                         * 
                         * Ecrire les instructions qui :
                         * initialise une variable de type Adhérent à partir des valeurs saisies
                         *  ajoute l'adhérent (la variable adhérent !) à la collection d'adhérents
                         * */
                        
                        Console.WriteLine();
                        Console.WriteLine("Saisir le numéro du nouvel adhérent");
                        numAdherent = Console.ReadLine();
                        unAdherent.NumeroAdherent = Convert.ToInt16(numAdherent);
                    
                        Console.WriteLine();
                        Console.WriteLine("Saisir le nom du nouvel adhérent");
                        unAdherent.NomAdherent = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Saisir le prénom du nouvel adhérent");
                        unAdherent.PrenomAdherent = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Saisir l'adresse email du nouvel adhérent");
                        unAdherent.EmailAdherent = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Saisir l'adresse postale du nouvel adhérent");
                        unAdherent.AdresseAdherent = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Saisir la ville de résidence du nouvel adhérent");
                        unAdherent.VilleAdherent = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine("Saisir le code postal de résidence du nouvel adhérent");
                        unAdherent.CodePostalAdherent = Console.ReadLine();

                        collectionAdherents.Add(unAdherent);
                        Console.ReadKey();
                        break;

                    case 3:
                        Console.WriteLine();
                        /* A compléter
                        * 
                        * Ecrire les instructions qui :    
                        * parcours la collection d'adhérents et affiche à chaque itération, les informations de l'adhérents courant
                        * */
                        Console.WriteLine();
                        foreach (Adherent adherantCourant in collectionAdherents)
                        {
                            Console.WriteLine("Numéro de l'adhérant:{0}",adherantCourant.NumeroAdherent);
                            Console.WriteLine("Nom de l'adhérant:{0}",adherantCourant.NomAdherent);
                            Console.WriteLine("Prenom de l'adhérant:{0}",adherantCourant.PrenomAdherent);
                            Console.WriteLine("mail de l'adhérant:{0}",adherantCourant.EmailAdherent);
                            Console.WriteLine("adresse postale de l'adhérant:{0}",adherantCourant.AdresseAdherent);
                            Console.WriteLine("ville de l'adhérant:{0}",adherantCourant.VilleAdherent);
                            Console.WriteLine("code postale de l'adhérant:{0}",adherantCourant.CodePostalAdherent);
                        }
                        
                        Console.WriteLine("");
                        Console.ReadKey();
                        break;

                    case 4:

                        /*
                         * Modifier le bloc d'instruction ci-dessous comme suit :
                         * Si aucun livre n'est référencé, le programme en informe l'utilisateur
                         * dans le cas contraire elle affiche la collection de livres
                         * 
                            */
                        
                        Console.WriteLine("Veuillez saisir l'ISBN du livre souhaité");
                        string chaineSaisie = Console.ReadLine();
                        foreach (Livre livreCourant in collectionLivres)
                        {
                            if (chaineSaisie == livreCourant.Isbn)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Numéro du livre:{0} ", livreCourant.NumLivre);
                                Console.WriteLine("Titre du livre:{0} ", livreCourant.TitreLivre);
                                Console.WriteLine("Auteur du livre:{0} ", livreCourant.Auteur);
                                Console.WriteLine("ISBN du livre:{0} ", livreCourant.Isbn);
                                Console.WriteLine();
                            }
                        }
                            if (chaineSaisie != unLivre.Isbn)
                            {
                                Console.WriteLine("Le numéro ISBN de ce livre n'existe pas");
                            }
                        
                        Console.ReadKey();
                        break;

                    case 5:

                        /* A compléter
                        * 
                        * Ecrire les instructions qui :    
                        * parcours la collection d'adhérents et 
                        *   Supprime l'adhérent lorsque le nom de l'adhérent parcouru (courant) est équivalent à celui saisi par l'utilisateur
                        *   Rappel : Une collection (LIST) parcouru à l'aide d'un FOREACH est en lecture seule. 
                        *   La méthode Remove de la classe LIST peut etre utilisée à condition de sortir prématurément 
                        *   du parcours à l'aide d'une BREAK
                        *   
                        *   
                        *   Vous proposerez deux parcours (itération) différents, l'un avec la structure foreach, l'autre avec la structure While
                        * */

                        
                        
                        
                        Console.WriteLine("Saisir le nom de l'adhérent a supprimer");
                        string nom = Console.ReadLine();
                        Console.WriteLine("Saisir le prénom de l'adhérent a supprimer");
                        string prenom = Console.ReadLine();
                        foreach (Adherent adherentCourant in collectionAdherents)
                        {
                            if (adherentCourant.NomAdherent == nom && adherentCourant.PrenomAdherent == prenom)
                            {
                                Console.WriteLine("L'adhérent à bien été supprimé");
                                collectionAdherents.Remove(adherentCourant);
                                Console.ReadKey();
                                break;                                
                            }
                            else 
                            {
                                Console.WriteLine("Le prénom ou nom de l'adhérent est incorrect");
                                Console.ReadKey();
                            }
                        }
                        Console.ReadKey();
                        
                      
                    case 6:

                        Console.Clear();
                        Console.WriteLine("Saisir le numéro ISBN du livre à supprimer");
                        isbn = Console.ReadLine();
                        nbElement = collectionLivres.Count; //nbElement contient le nombre de livres présent dans la collection
                        compteur = 0;//

                        /*A compléter
                        *
                        *Ecrire les instructions qui:
                        * 
                        * Parcours à l'aide d'une boucle While la collection de livres.
                        *   recherche l'indice correspondant à l'emplacement ou se trouve le livre dans la collection
                        *   supprime l'élément en s'appuyant sur la méthode "removeAt" de la classe LIST
                        *   
                        * L'application affichera un message de confirmation 
                        * ou un message indiquant l'absence du livre recherché dans la collection de livres référencés.
                         * */

                        Console.ReadKey();
                        break;

                    case 7:

                        Console.Clear();
                        /*A compléter
                        *
                        *Ecrire les instructions qui:
                        * 
                        * 1) Affiche la collection de livres référencés
                        * 2) Permet la saisie d'un numéro de livre [correspond à la sélection d'un livre]
                        * ou S pour sortir du traitement en cours [Utilisez le break pour sortir du case en cours]
                        * 3) Le systeme recherche "le livre" à partir de la valeur précédement saisie
                        * Si le numéro de livre est erroné, le "case" 7 est relancé [utilisez l'instruction goto]
                        * dans le cas contraire il est stocké dans la variable "unLivre" de type Livre
                        * 
                        * 4)Effectuer le traitement similaire permettant de sélectionner un adhérent
                        * Celui-ci sera stocké dans la variable "unAdherent" de type Adherent
                        * 
                        * 5) 
                        * */

                        Console.WriteLine("Saisir la date de retour prévue");
                        dateRetour = Console.ReadLine();
                        DateTime dt = Convert.ToDateTime(dateRetour);

                        /*A compléter
                        *
                        *Ecrire les instructions qui:
                         * 
                         * initialise un objet de type emprunt à partir des variables objets Livre (unLivre) 
                         * et Adherent (unAdherent) précédemment valorisées ainsi que la date d'emprunt stockée
                         * dans une structure (similaire à un objet) de type dateTime (dt)
                         * 
                         * Ajoute l'emprunt à la collection d'emprunt
                         * 
                         * */

                        break;

                    case 8:

                        Console.Clear();
                        foreach (Emprunt empruntCourant in collectionEmprunts)
                        {
                            Console.WriteLine("Date de l'emprunt : {0} \nDate de retour prévue {1} \nAdhérent : {2} \nLivre {3} ", empruntCourant.DateEmprunt.ToString(), empruntCourant.DateRetour.ToString(), empruntCourant.UnAdherent.NomAdherent, empruntCourant.UnLivre.TitreLivre);
                        }
                        Console.ReadKey();
                        break;

                    case 9:

                        /*A compléter
                        *
                        * Ecrire les instructions qui:
                        * 
                        * A partir d'un numéro d'adhérent et d'un numéro de livre, recherche l'emprunt correspondant
                        * et enregistre son retour.
                        * 
                        * */
                        break;

                    case 0:

                        #region SAUVEGARDE DES DONNEES
                        Persistance.Sauvegarde(collectionLivres);
                        Persistance.Sauvegarde(collectionEmprunts);
                        Persistance.Sauvegarde(collectionAdherents);
                        #endregion

                        sortir = true;
                        break;

                    default:
                        sortir = true;
                        break;


                }

            } while (!sortir);

        }

    }

}

