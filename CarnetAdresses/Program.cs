﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CarnetAdresses
{
    class Program
    {
        static void Main(string[] args)
        {

            string ligne;
            List<string[]> contacts;
            contacts = new List<string[]>();

            string cheminFichier = Environment.CurrentDirectory + @"\noms.txt";
            StreamReader lecteurDeFichier = new StreamReader(cheminFichier);

            ligne = lecteurDeFichier.ReadLine();
            while (ligne != null)
            {
                
                char[] tabCaractere = { ';' };
                string[] unContact = ligne.Split(tabCaractere);
                contacts.Add(unContact);
                ligne = lecteurDeFichier.ReadLine();
            }
            lecteurDeFichier.Close();


            bool continuer = true;
            do
            {
                Console.WriteLine("0-fermer le programme");
                Console.WriteLine("1-Afficher les contacts");
                Console.WriteLine("2-Ajouter un contact");


                string choix = Console.ReadLine();
                switch (choix)

                {
                    
                    
                    case "1":

                        foreach (string[] contact in contacts)
                        {
                            Console.Write("nom: ");
                            Console.Write(contact[0]);
                            Console.Write("; ");
                            Console.Write("tel: ");
                            Console.Write(contact[1]);
                            Console.Write("; ");
                            Console.Write("adresse: ");
                            Console.WriteLine(contact[2]);
                            Console.WriteLine("");
                          
                        }
                        break;

                        case"2":

                        Console.WriteLine("nom du contact:");
                        string nom = Console.ReadLine();
                        string tel ;                       
                        bool numeroValide;
                        do
                        {
                            numeroValide = true;
                            Console.WriteLine("Telephone du contact");
                            tel = Console.ReadLine();
                            foreach (char caractere in tel)
                            {
                                if (char.IsDigit(caractere) == false)
                                {                                    
                                    numeroValide = false;
                                }                                
                            }
                            Console.WriteLine("Veuillez saisir un numéro composé uniquement de chiffres");

                            if (tel.Length != 10)
                            {
                                Console.WriteLine("Veuillez saisir un numéro composé de 10 chiffres");
                                numeroValide =false;
                            }
                            
                        }
                        while (numeroValide == false);

                        Console.WriteLine("adresse du contact:");
                        string adresse = Console.ReadLine();
                        contacts.Add(new string[] { nom, tel, adresse });


                        break;
                    case "0":
                        continuer = false;
                        StreamWriter redacteurFichier = new StreamWriter(cheminFichier);
                        foreach (string[] contact in contacts)
                        {
                            string unContact = contact[0] + ";" + contact[1] + ";" + contact[2];
                            redacteurFichier.WriteLine(unContact);
                        }
                        redacteurFichier.Close();
                        break;
                    default:
                        break;

                }
            }
            
            while (continuer == true);









        }
    }
}
