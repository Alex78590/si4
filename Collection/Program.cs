﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    class Program
    {
        static void Main(string[] args)
        {
            //Déclarer collection
            List<string> listeCapitale;
            listeCapitale = new List<string>();

            //ajouter éléments
            string uneChaine = "Paris";
            listeCapitale.Add(uneChaine);
            listeCapitale.Add("Madrid");
            listeCapitale.Add("Stockholm");
            listeCapitale.Add("Londres");
            listeCapitale.Add("Anvers");

            //supression 
            bool suppression = listeCapitale.Remove("Madrid");

            //Parcourir collection
            foreach (string capitaleCourante in listeCapitale)
            {
                Console.WriteLine(capitaleCourante);
            }

            //rechercher un élément
            Console.WriteLine("Saisir une capitale européenne");
            string capitaleSaisie = Console.ReadLine();
            foreach (string capitaleCourante in listeCapitale) 
            {
                if (capitaleCourante == capitaleSaisie)
                {
                    break;
                }
            }

            //Vérifier l'existence d'un élément
            bool trouve = listeCapitale.Contains("Rome");

            //notation indéxer
            string capitale = listeCapitale[0];
            Console.WriteLine(capitale);

            //parcourir une collection avec une boucle for:
            for (int indice = 0; indice < listeCapitale.Count; indice++) 
            {
                Console.WriteLine(listeCapitale[indice]);
            }































        }
    }
}
