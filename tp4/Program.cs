﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //exo 1 
            /* int[] valeurs = new int[10];
              String nombre;
              Console.WriteLine("veuillez saisir 10 nombres");
              for (int i = 0; i < 10; i++) 
              {
                  nombre = Console.ReadLine();
                  valeurs[i] = Convert.ToInt32(nombre);
              }
              Console.ReadLine();
              */
            // exo 2
            /*int[] valeurs = new int[10];
            String nombre;
            Console.WriteLine("veuillez saisir 10 nombres");
            for (int i = 0; i < 10; i++)
            {
                nombre = Console.ReadLine();
                valeurs[i] = Convert.ToInt32(nombre);
            }
            for (int j = 0; j < 10; j++)
            {
                Console.WriteLine(valeurs[j]);
            }
            Console.ReadLine();
            */


            //exo 3
            /* string nombre;
             int[] tableau;
             tableau = new int[11];
             for (int i = 1; i < tableau.Length; i++)
             {
                 Console.WriteLine("Veuillez entrer un nombre", i);
                 nombre = Console.ReadLine();
                 tableau[i] = Convert.ToInt32(nombre);
                 if (i % 2 == 0)
                 {
                     Console.WriteLine("Vous avez stocké la valeur {0} dans la colonne {1} du tableau ", tableau[i], i);
                 }
             }
             */

            //exo4
            /* string nombre;
             int[] tableau;
             tableau = new int[10];
             for (int i = 0; i < tableau.Length; i++)
             {
                 Console.WriteLine("Veuillez entrer un nombre", i);
                 nombre = Console.ReadLine();
                 tableau[i] = Convert.ToInt32(nombre);

                 }
                 for (int w = 1; w < 10 ;w++)
                 {
                 if (tableau[w] % 2 == 0)
                 {
                     Console.WriteLine("Vous avez stocké la valeur {0} dans la colonne {1} du tableau ", tableau[w], w);


                 }
             }
             Console.ReadLine();
             */


            //exo5
            /*string nombre;
            int[] tableau;
            tableau = new int[10];
            for (int i = 0; i < tableau.Length; i++)
            {
                Console.WriteLine("Veuillez entrer un nombre", i);
                nombre = Console.ReadLine();
                tableau[i] = Convert.ToInt32(nombre);
                Console.WriteLine("Vous avez stocké la valeur {0} dans le tableau ", tableau[i]);

            }
            Console.WriteLine("la valeur maximale est {0}", tableau.Max());
            Console.ReadLine();
            */

            //exo6 
            /* string nombre;
             int[] tableau;
             tableau = new int[10];
             for (int i = 0; i < tableau.Length; i++)
             {
                 Console.WriteLine("Veuillez entrer un nombre", i);
                 nombre = Console.ReadLine();
                 tableau[i] = Convert.ToInt32(nombre);
                 Console.WriteLine("Vous avez stocké la valeur {0} dans le tableau ", tableau[i]);

             }
             Console.WriteLine("la valeur minimal est {0}", tableau.Min());
             Console.ReadLine();
             */

            //exo 7
            /*string nombre;
            int[] tableau;
            int somme;
            somme = 0;
            tableau = new int[10];
            for (int i = 0; i < tableau.Length; i++)
            {
                Console.WriteLine("Veuillez entrer un nombre", i);
                nombre = Console.ReadLine();
                tableau[i] = Convert.ToInt32(nombre);
                Console.WriteLine("Vous avez stocké la valeur {0} dans le tableau ", tableau[i]);
                somme = somme + tableau[i];
            }
            Console.WriteLine("La somme des valeurs est de {0}", somme);
            Console.ReadLine();
            */

            //exo 8
            /* string nombre;
             int[] tableau;
             int somme;
             int moyenne;
             somme = 0;
             moyenne = 0;
             tableau = new int[10];
             for (int i = 0; i < tableau.Length; i++)
             {
                 Console.WriteLine("Veuillez entrer un nombre", i);
                 nombre = Console.ReadLine();
                 tableau[i] = Convert.ToInt32(nombre);
                 Console.WriteLine("Vous avez stocké la valeur {0} dans le tableau ", tableau[i]);
                 somme = somme + tableau[i];

             }
             moyenne = somme / tableau.Length;
             Console.WriteLine("La moyenne des valeurs est de {0}", moyenne);
             Console.ReadLine();
             */



            //ordre croissant et decroissant
            /* string nombre;
             int[] tableau; 
             tableau = new int[10];
             for (int i = 0; i < tableau.Length; i++)
             {
                 Console.WriteLine("Veuillez entrer un nombre", i);
                 nombre = Console.ReadLine();
                 tableau[i] = Convert.ToInt32(nombre);
             }
             Console.WriteLine("L'ordre croissant est");
             Array.Sort(tableau); foreach (var i in tableau)
             {

                 Console.WriteLine(i + " ");

             }
             Console.WriteLine("L'ordre décroissant est");
             Array.Reverse(tableau); foreach (var i in tableau)
             {
                 Console.WriteLine(i + " ");
             }
                 Console.ReadLine();
                 */

            //ordre croissant 2

            /* int[] tableau = new int[20];
             Random rnd = new Random();
             for (int j = 0; j < tableau.Length; j++)
             {
                 tableau[j] = rnd.Next(1, 100);
             }

             int nombre1, nombre2;
             for (int y = tableau.Length; y > 0; y--)
             {
                 for (int i = 0; i < y - 1; i++)
                 {
                     if (tableau[i] >  tableau[i+1])
                     {
                         nombre1 = tableau[i];
                         nombre2 = tableau[i + 1];
                         tableau[i] = nombre2;
                         tableau[i + 1] = nombre1;
                         Console.WriteLine(tableau[i]);
                         Console.WriteLine(tableau[i + 1]);

                     }
                 }
             }
             Console.WriteLine("ordre croissant:");
                      for (int w=0; w<tableau.Length;w++)
             {

                 Console.WriteLine(tableau[w]);
             }

     */

             //test
           /* int a, b, i;
            string valeurSaisie;
            Console.WriteLine("veuillez saisir un nombre a");
            Console.WriteLine("veuillez saisir un nombre b");
            valeurSaisie = Console.ReadLine();
            a = Convert.ToInt32(valeurSaisie);
            valeurSaisie = Console.ReadLine();
            b = Convert.ToInt32(valeurSaisie);
            i = 0;
            
            while(a>=b)
            { a = a - b;
                i++;
            }
            Console.WriteLine("Le resultat est {0}", i);
            */

            //puissance
            /*int N,i=0,j=1,y=0;
            string valeurSaisie;
            Console.WriteLine("Veuillez saisirle nombre de fois ou on additionne les puissance de N et sa valeur");
            valeurSaisie = Console.ReadLine();
            N = Convert.ToInt32(valeurSaisie);
            for ( i = 1; i<= N; i++) 
            {
                j = j* N;
                y=y+j;
            }
            y = y + 1;
            Console.WriteLine("le résultat est {0}", y);
            */
            





            Console.ReadLine();
















        }
    }
}

















